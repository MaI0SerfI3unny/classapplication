﻿#include <iostream>
#include <cmath>

using namespace std;

class Vector {
private:
    double x;
    double y;
    double z;
public:
    Vector(double x, double y, double z) 
        : x(x), y(y), z(z) { }

    double getX(){
        return x;
    }

    double getY(){
        return y;
    }

    double getZ(){
        return z;
    }

    double getLength(){
        return sqrt(x * x + y * y + z * z);
    }
};

int main() {
    Vector v(5, 12, 43);

    cout << "x: " << v.getX() << endl;
    cout << "y: " << v.getY() << endl;
    cout << "z: " << v.getZ() << endl;
    cout << "Length: " << v.getLength() << endl;

    return 0;
}